// Retrieve the carYearsList from the problem4  
const carYearslist = require('./problem4');
const carYears = carYearslist(inventory);

// Function to return an array of cars older than 2000 and logs its length
const olderCars = (inventory, carYears) => {
    let oldCars = [];
    if (Array.isArray(inventory) && inventory.length && Array.isArray(carYears) && carYears.length){
        for (let i = 0; i < carYears.length; i++) {
            if (carYears[i] < 2000) {
                oldCars.push(carYears[i]);
            }
        }
    }
    console.log(`The no. of cars older than 2000 are: ${oldCars.length}`);
    return oldCars;
}

module.exports = olderCars;
