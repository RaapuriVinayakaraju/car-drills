// import the inventory data from the file cars.js
const inventory = require('./cars');

// The function returns the car models sorted in alphabetic order
const carModelsList = (inventory) => {
    let car_models = [];
    if (Array.isArray(inventory) && inventory.length){
        for (let i = 0; i < inventory.length; i++) {
            car_models.push(inventory[i].car_model);
        }
    }
    return car_models.sort();
}

module.exports = carModelsList;
