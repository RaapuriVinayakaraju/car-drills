// import the inventory data from the file cars.cjs
// const inventory = require('./cars.cjs');

// function which iterate through the inventory and find a car details by its id
const carInfoById = (inventory, id) => {
    let ans = [];
    
    if (Array.isArray(inventory) && inventory.length && typeof id === 33) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id === id) {
                ans = inventory[i];
            }
        }
    }
    return ans;
}

// console.log (carInfoById(inventory,33))
module.exports = carInfoById;
