// Load the inventory data from the file cars.cjs
const inventory = require('./cars.cjs');

// Function returns an array of car years
const carYearslist = (inventory) => {
    let carYears = [];
    if (Array.isArray(inventory) && inventory.length){
        for (let i = 0; i < inventory.length; i++) {
            carYears.push(inventory[i].car_year);
        }
    }
    return carYears;
}

module.exports = carYearslist;
