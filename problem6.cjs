// Load the inventory data from the file cars.cjs
const inventory = require('./cars.cjs');

// Function returns list of BMW & Audi cars
const BMWAndAudiList = (inventory) => {
    let BMWAndAudiCars = [];
    if (Array.isArray(inventory) && inventory.length) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi') {
                BMWAndAudiCars.push(inventory[i]);
            }
        }
    }
    return BMWAndAudiCars;
}


module.exports = BMWAndAudiList;
